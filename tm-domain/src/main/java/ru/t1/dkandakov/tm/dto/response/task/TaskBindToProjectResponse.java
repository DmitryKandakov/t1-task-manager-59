package ru.t1.dkandakov.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {
}
