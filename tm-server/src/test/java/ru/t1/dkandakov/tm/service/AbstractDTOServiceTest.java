package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.dkandakov.tm.configuration.ServerConfiguration;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.marker.DBCategory;

import java.util.Arrays;
import java.util.List;

@Category(DBCategory.class)
public abstract class AbstractDTOServiceTest {

    @NotNull
    protected final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    protected final IUserServiceDTO userService = context.getBean(IUserServiceDTO.class);
    @NotNull
    protected final IProjectServiceDTO projectService = context.getBean(IProjectServiceDTO.class);
    @NotNull
    protected final ITaskServiceDTO taskService = context.getBean(ITaskServiceDTO.class);
    @NotNull
    protected final ISessionServiceDTO sessionService = context.getBean(ISessionServiceDTO.class);
    @NotNull
    protected IPropertyService propertyService = context.getBean(IPropertyService.class);

    @After
    public void clearAllModel() {
        taskService.removeAll();
        projectService.removeAll();
        userService.removeAll();
    }

    @Before
    public void init() {
        @NotNull final UserDTO user1 = userService.create("user1", "user1", "user1@test.ru");
        user1.setId("userId-1");
        @NotNull final UserDTO user2 = userService.create("user2", "user2", "user2@test.ru");
        user2.setId("userId-2");
        @NotNull final List<UserDTO> list = Arrays.asList(user1, user2);
        userService.removeAll();
        userService.set(list);
    }

}
