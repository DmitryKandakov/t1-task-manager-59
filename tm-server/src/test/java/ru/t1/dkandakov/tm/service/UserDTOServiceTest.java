package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.marker.DBCategory;

@Category(DBCategory.class)
public class UserDTOServiceTest extends AbstractDTOServiceTest {

    @Test
    @Category(DBCategory.class)
    public void testClear() {
        userService.removeAll();
        userService.create("test1", "test1", "test@test.ru");
        userService.create("test2", "test2", "test2@test.ru");
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() {
        userService.removeAll();
        userService.create("test1", "test1", "test@test.ru");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals("test1", userService.findAll().get(0).getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() {
        userService.removeAll();
        userService.create("test1", "test1", "test@test.ru");
        userService.create("test2", "test2", "test2@test.ru");
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindByEmail() {
        userService.removeAll();
        @NotNull final UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertEquals(user.getId(), userService.findByEmail("test@test.ru").getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindByLogin() {
        userService.removeAll();
        @NotNull final UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertEquals(user.getId(), userService.findByLogin("test1").getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() {
        userService.removeAll();
        @NotNull final UserDTO user1 = userService.create("test1", "test1", "test@test.ru");
        @NotNull final UserDTO user2 = userService.create("test2", "test2", "test2@test.ru");
        @NotNull final String userId1 = user1.getId();
        @NotNull final String userId2 = user2.getId();
        Assert.assertEquals("test1", userService.findOneById(userId1).getLogin());
        Assert.assertEquals("test2", userService.findOneById(userId2).getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testIsEmailExists() {
        userService.removeAll();
        @NotNull final UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertTrue(userService.isEmailExist("test@test.ru"));
        Assert.assertFalse(userService.isEmailExist("another@email.ru"));
    }

    @Test
    @Category(DBCategory.class)
    public void testIsLoginExists() {
        userService.removeAll();
        @NotNull final UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertTrue(userService.isLoginExist("test1"));
        Assert.assertFalse(userService.isLoginExist("anotherLogin"));
    }

    @Test
    @Category(DBCategory.class)
    public void testLockByLogin() {
        userService.removeAll();
        @NotNull UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertFalse(user.isLocked());
        userService.lockOneByLogin("test1");
        user = userService.findByLogin("test1");
        Assert.assertTrue(user.isLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnlockByLogin() {
        userService.removeAll();
        @NotNull UserDTO user = userService.create("test1", "test1", "test@test.ru");
        userService.lockOneByLogin("test1");
        user = userService.findByLogin("test1");
        Assert.assertTrue(user.isLocked());
        userService.unlockOneByLogin("test1");
        user = userService.findByLogin("test1");
        Assert.assertFalse(user.isLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveByLogin() {
        userService.removeAll();
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final UserDTO user = userService.create("test1", "test1", "test@test.ru");
        Assert.assertNotNull(userService.findByLogin("test1"));
        Assert.assertEquals(user.getId(), userService.removeOneByLogin("test1").getId());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateUser() {
        userService.removeAll();
        @NotNull UserDTO user = userService.create("test1", "test1", "test@test.ru");
        @NotNull final String userId = user.getId();
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, "First Name", "Last Name", "Middle Name");
        user = userService.findByLogin("test1");
        Assert.assertEquals("First Name", user.getFirstName());
        Assert.assertEquals("Last Name", user.getLastName());
        Assert.assertEquals("Middle Name", user.getMiddleName());
    }

}
