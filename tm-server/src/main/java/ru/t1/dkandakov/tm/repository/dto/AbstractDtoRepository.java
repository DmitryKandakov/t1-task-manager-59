package ru.t1.dkandakov.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkandakov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
