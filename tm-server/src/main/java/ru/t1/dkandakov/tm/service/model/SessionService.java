package ru.t1.dkandakov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkandakov.tm.api.repository.model.ISessionRepository;
import ru.t1.dkandakov.tm.api.service.model.ISessionService;
import ru.t1.dkandakov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkandakov.tm.model.Session;
import ru.t1.dkandakov.tm.repository.model.SessionRepository;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    public SessionRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

}
