package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;


public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}