package ru.t1.dkandakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.listener.AbstractListener;

@Component
public final class ApplicationCommandListListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@applicationCommandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@Nullable final AbstractListener abstractListener : listeners) {
            if (abstractListener == null) continue;
            @Nullable final String name = abstractListener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

}