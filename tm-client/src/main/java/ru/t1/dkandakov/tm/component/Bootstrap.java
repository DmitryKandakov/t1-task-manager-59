package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.endpoint.*;
import ru.t1.dkandakov.tm.api.service.ILoggerService;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.api.service.IServiceLocator;
import ru.t1.dkandakov.tm.api.service.ITokenService;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.listener.AbstractListener;
import ru.t1.dkandakov.tm.util.SystemUtil;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.dkandakov.tm.command";

    @Getter
    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;


    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    private void prepareStartup() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER HAS DONE **");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        publisher.publishEvent(new ConsoleEvent(command));
    }

    public void start(@Nullable final String[] args) {
        prepareStartup();
        runByCommand();
    }

    private void runByCommand() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
