package ru.t1.dkandakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void handler(ConsoleEvent consoleEvent);

}
