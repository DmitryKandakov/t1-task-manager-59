package ru.t1.dkandakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskListRequest;
import ru.t1.dkandakov.tm.dto.response.task.TaskListResponse;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.TaskSort;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskShowListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskShowListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort taskSort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), taskSort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);
        renderTasks(response.getTasks());
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list.";
    }

}