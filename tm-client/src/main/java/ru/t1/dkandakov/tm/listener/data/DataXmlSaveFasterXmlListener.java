package ru.t1.dkandakov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataXmlSaveFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

@Component
public class DataXmlSaveFasterXmlListener extends AbstractDataListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
