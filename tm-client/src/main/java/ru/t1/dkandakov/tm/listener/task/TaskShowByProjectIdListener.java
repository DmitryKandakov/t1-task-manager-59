package ru.t1.dkandakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.dkandakov.tm.dto.response.task.TaskListByProjectIdResponse;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request =
                new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        renderTasks(response.getTasks());
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project id";
    }

}